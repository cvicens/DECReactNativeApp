import React, { Component } from 'react'
import { Navigator } from 'react-native'
import { View, Text, Button, ActivityIndicator, ScrollView, TouchableOpacity, StyleSheet} from 'react-native'
import { connect } from 'react-redux'

//import { NativeModules } from 'react-native';
//var RCTFH = NativeModules.FH;

import { actionCreators } from './postsRedux'

import Login from './scenes/Login'
import GenericForm from './scenes/GenericForm'
import Form1 from './scenes/Form1'

import Components from './scenes/Components'

const mapStateToProps = (state) => ({
  loading: state.posts.loading,
  error: state.posts.error,
  posts: state.posts.posts,
  init: state.posts.init,
  authenticated: state.posts.authenticated,
  userId: state.posts.userId
})

class App extends Component {
  componentDidMount () {
    // After the component mounts we refresh to request data
    this.init();
  }


  componentWillMount() {

    const {dispatch} = this.props
    // Before the component mounts we refresh to request data
    //this.refresh();
  }


  init = async () => {
    const {dispatch, init} = this.props;

    if (!init) {
      try {
        await dispatch(actionCreators.init());
        //await dispatch(actionCreators.auth('wfm', 'max', '123'));
      } catch (e) {
        console.log('ERROR:', e);
      }  
    }

    // We can await the completion of dispatch, so long as we returned a promise.
    //await dispatch(actionCreators.clearPosts());
    //dispatch(actionCreators.fetchPosts());
  }

  renderScene(route, navigator) {
    _navigator = navigator;
    switch (route.id) {
      case 'login':
        return (<Login navigator={navigator} title="Login"/>);
      case 'form1':
        //return (<GenericForm navigator={navigator} title="Form 1" />);
        return (<Form1 navigator={navigator} title="Form 1" />);
      case 'generic':
        return (<GenericForm navigator={navigator} title="Form 1" />);
    }
  }
  

  render() {
    const {posts, loading, error, init, authenticated, userId} = this.props

    return (
      <Navigator
        style={styles.container}
        initialRoute={{id: 'login'}}
        //initialRoute={{id: 'generic'}}
        configureScene={(route) => Navigator.SceneConfigs.FloatFromBottom}
        renderScene={this.renderScene}/>
    );

    /*if (!authenticated) {
      return (
        <Login />
      )
    } else {
      return (
        <GenericForm />
      )
    }*/
  }
}

const styles = StyleSheet.create({
  login: {
    flex: 3,
  },
  container: {
    flex: 1,
  },
  post: {
    flexDirection: 'row',
  },
  postNumber: {
    width: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  postContent: {
    flex: 1,
    borderBottomWidth: 1,
    borderBottomColor: '#EEE',
    paddingVertical: 25,
    paddingRight: 15,
  },
  postBody: {
    marginTop: 10,
    fontSize: 12,
    color: 'lightgray',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    height: 80,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: 'lightgray',
  },
  // tests layout
   layout: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.05)',
  },
  box: {
    padding: 25,
    backgroundColor: 'steelblue',
    margin: 5,
  }
})

export default connect(mapStateToProps)(App)
