import React, { Component } from 'react';

import { AppRegistry, View } from 'react-native'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'

// Import the reducer and create a store
import { posts } from './postsRedux'
import { login } from './scenes/loginRedux'
import { form } from './scenes/genericFormRedux'

import { components } from './scenes/componentsRedux'

var reducer = combineReducers({posts, login, form, components});

// Add the thunk middleware to our store
const store = createStore(reducer, applyMiddleware(thunk))
console.log('store', store.getState());

// Import the App container component
import App from './App'

// Pass the store into the Provider
const AppWithStore = () => (
  <Provider store={store}>
    <App />
  </Provider>
)

AppRegistry.registerComponent('DECReactNativeApp', () => AppWithStore)
