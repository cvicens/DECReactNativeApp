var RCTFH = require('rct-fh');
//import { NativeModules } from 'react-native';
//var RCTFH = NativeModules.FH;
//console.log('NativeModules', NativeModules);

export const types = {
  FH_SDK_AUTH_REQUEST: 'FH_SDK_AUTH_REQUEST',
  FH_SDK_AUTH_RESPONSE: 'FH_SDK_AUTH_RESPONSE',
  FH_SDK_INIT_REQUEST: 'FH_SDK_INIT_REQUEST',
  FH_SDK_INIT_RESPONSE: 'FH_SDK_INIT_RESPONSE',
  FETCH_POSTS_REQUEST: 'FETCH_POSTS_REQUEST',
  FETCH_POSTS_RESPONSE: 'FETCH_POSTS_RESPONSE',
  CLEAR_POSTS: 'CLEAR_POSTS',
}

export const actionCreators = {
  init: () => async (dispatch, getState) => {
    console.log('init FH SDK', dispatch, getState);
    dispatch({type: types.FH_SDK_INIT_REQUEST});

    try {
        const result = await RCTFH.init();
        console.log('init result', result)

        if (result === 'SUCCESS') {
          dispatch({type: types.FH_SDK_INIT_RESPONSE});
        } else {
          dispatch({type: types.FH_SDK_INIT_RESPONSE, payload: result, error: true});
        }
      } catch (e) {
        dispatch({type: types.FH_SDK_INIT_RESPONSE, payload: e, error: true});
      }
  },

  auth: (authPolicy, username, password) => async (dispatch, getState) => {
    console.log('auth FH SDK', dispatch, getState);
    dispatch({type: types.FH_SDK_AUTH_REQUEST});

    try {
        const result = await RCTFH.auth(authPolicy, username, password);
        console.log('auth result', result);
        if (typeof result.sessionToken !== 'undefined') {
          dispatch({type: types.FH_SDK_AUTH_RESPONSE, payload: result});
        } else {
          dispatch({type: types.FH_SDK_AUTH_RESPONSE, payload: result, error: true});
        }
      } catch (e) {
        dispatch({type: types.FH_SDK_AUTH_RESPONSE, payload: e, error: true});
      }
  },

  fetchPostsOld: () => async (dispatch, getState) => {
    console.log('fetchPostsOld', dispatch, getState);
    dispatch({type: types.FETCH_POSTS_REQUEST})

    try {
      const response = await fetch('https://jsonplaceholder.typicode.com/posts')
      const posts = await response.json()

      //console.log('after response received', posts);

      dispatch({type: types.FETCH_POSTS_RESPONSE, payload: posts})
    } catch (e) {
      dispatch({type: types.FETCH_POSTS_RESPONSE, payload: e, error: true})
    }
  },

  fetchPosts: () => async (dispatch, getState) => {
    console.log('fetchPosts', dispatch, getState);
    dispatch({type: types.FETCH_POSTS_REQUEST})

    try {
      const todos = await RCTFH.cloud({
        "path": "/todos", //only the path part of the url, the host will be added automatically
        "method": "GET", //all other HTTP methods are supported as well. For example, HEAD, DELETE, OPTIONS
        "contentType": "application/json",
        "data": { "hello": "Carlos"}, //data to send to the server
        "timeout": 25000 // timeout value specified in milliseconds. Default: 60000 (60s)
      });

      dispatch({type: types.FETCH_POSTS_RESPONSE, payload: todos})
    } catch (e) {
      dispatch({type: types.FETCH_POSTS_RESPONSE, payload: e, error: true})
    }
  },

  // It's common for action creators to return a promise for easy chaining,
  // which is why this is declared async (async functions always return promises).
  clearPosts: () => async (dispatch, getState) => {
    if (getState().posts.length > 0) {
      dispatch({type: types.CLEAR_POSTS})
    }
  }
}

const initialState = {
  loading: true,
  init: false,
  authenticated: false,
  userId: '',
  error: false,
  errorMessage: '',
  posts: [],
}

export const posts = (state = initialState, action) => {
  const {todos} = state
  const {type, payload, error} = action

  switch (type) {
     case types.FH_SDK_INIT_REQUEST: {
      return {...state, loading: true, init: false, error: false}
    }
    case types.FH_SDK_INIT_RESPONSE: {
      if (error) {
        return {...state, loading: false, init: false, error: true, errorMessage: payload.message}
      }

      return {...state, loading: false, init: true, error: false}
    }

    case types.FH_SDK_AUTH_REQUEST: {
      return {...state, loading: true, authenticated: false}
    }
    case types.FH_SDK_AUTH_RESPONSE: {
      if (error) {
        return {...state, loading: false, authenticated: false, error: true}
      }
      return {...state, loading: false, authenticated: true, error: false, userId: payload.userId}
    }
   
    case types.FETCH_POSTS_REQUEST: {
      return {...state, loading: true, error: false}
    }
    case types.FETCH_POSTS_RESPONSE: {
      if (error) {
        return {...state, loading: false, error: true}
      }

      return {...state, loading: false, posts: payload}
    }
  }

  return state
}
