import React, { Component } from 'react'
import { View, ScrollView, Text, TextInput, Image, Button, Picker, DatePickerIOS, ActivityIndicator, StyleSheet} from 'react-native'
import { connect } from 'react-redux'

import { FEED_BACK_FORM_ID } from './constants'

import { actionCreators } from './componentsRedux'

export const types = {
  radio: 'radio',
  textarea: 'textarea',
  dateTime: 'dateTime',
}

const mapStateToProps = (state) => ({
  loading: state.form.loading,
  init: state.form.init,
  formId: state.form.formId,
  formMetadata: state.form.formMetadata,
  formFields: state.form.formFields,
  error: state.form.error,
  errorMessage: state.form.errorMessage,
})

class DatePickerExample extends Component {
  static defaultProps = {
    date: new Date(),
    timeZoneOffsetInHours: (-1) * (new Date()).getTimezoneOffset() / 60,
  };

  state = {
    date: this.props.date,
    timeZoneOffsetInHours: this.props.timeZoneOffsetInHours,
  };

  onDateChange = (date) => {
    this.setState({date: date});
  };

  onTimezoneChange = (event) => {
    var offset = parseInt(event.nativeEvent.text, 10);
    if (isNaN(offset)) {
      return;
    }
    this.setState({timeZoneOffsetInHours: offset});
  };

  render() {
    // Ideally, the timezone input would be a picker rather than a
    // text input, but we don't have any pickers yet :(
    return (
      <View style={styles.container}>
        <DatePickerIOS
          date={this.state.date}
          mode="datetime"
          timeZoneOffsetInMinutes={this.state.timeZoneOffsetInHours * 60}
          onDateChange={this.onDateChange}
        />
        <DatePickerIOS
          date={this.state.date}
          mode="date"
          timeZoneOffsetInMinutes={this.state.timeZoneOffsetInHours * 60}
          onDateChange={this.onDateChange}
        />
        <DatePickerIOS
          date={this.state.date}
          mode="time"
          timeZoneOffsetInMinutes={this.state.timeZoneOffsetInHours * 60}
          onDateChange={this.onDateChange}
          minuteInterval={10}
        />
      </View>
    );
  }
}

class GenericForm extends Component {
  componentDidMount () {
    console.log('GenericForm.componentDidMount');
  }

  componentWillMount() {
    console.log('GenericForm.componentWillMount');
    const {dispatch} = this.props
    this.init();
  }

  init = async () => {
    const {dispatch} = this.props;

    // We can await the completion of dispatch, so long as we returned a promise.
    await dispatch(actionCreators.init(FEED_BACK_FORM_ID));
  }

  submitFormData = async () => {
    const {dispatch} = this.props;

    // We can await the completion of dispatch, so long as we returned a promise.
    await dispatch(actionCreators.submitFormData(FEED_BACK_FORM_ID));
  }

  updateUsername = async (username) => {
    const {dispatch} = this.props;
    await dispatch(actionCreators.updateUsername(username));
  }

  updatePassword = async (password) => {
    const {dispatch} = this.props;
    await dispatch(actionCreators.updatePassword(password));
  }

  updateProperty = async (propertyValue) => {
    console.log('updateProperty->', propertyValue);
  }

  render() {
    const {loading, error, errorMessage, init, formMetadata, formFields} = this.props;

    console.log('GenericForm.render this.props', this.props);
    console.log('GenericForm.render formMetadata', formMetadata);
    console.log('GenericForm.render formFields', formFields);

    let formComponent = formFields.map((field, i) => {
      switch (field.type) {
        case types.radio: {
          var options = field.fieldOptions.definition.options;
          let items = options.map((option, j) => {
            return (<Picker.Item label={option.label} value={option.label} key={i+j}/>);
          });
          console.log('options', options, 'items', items);
          return (
            <View style={{flex: 1, flexDirection: 'column', alignItems: 'flex-start',
      justifyContent: 'center'}}>
            <Text style={styles.pickerHeader}>{field.name}</Text>
            <View key={i+100} style={{flex: 1, flexDirection: 'row'}}>
              <Picker style={styles.picker} key={i}>
                {items}
              </Picker>
            </View>
            </View>
          );
        }
        case types.textarea: {
          return <TextInput key={i} style={styles.input} autoCapitalize = 'none'
                            placeholder={ field.name }/>
        }
        case types.dateTime: {
          return <DatePickerIOS key={i} style={{flex: 1, flexDirection: 'row'}}
            date={new Date()}
            mode="datetime"
            timeZoneOffsetInMinutes={0}
            onDateChange={(event) => this.updateProperty(event.nativeEvent)}
          />
        }
      }                            
    }); 
    console.log('form', formComponent);

    return (
     <ScrollView contentContainerStyle={styles.container}>
        {formComponent}
        <TextInput style={styles.input} autoCapitalize = 'none'
            onSubmitEditing={(event) => this.updateUsername(event.nativeEvent.text)}
            onEndEditing={(event) => this.updateUsername(event.nativeEvent.text)}
            placeholder='User Name'
          />
        <Button style={styles.button}
          disabled={loading}
          onPress={this.submitFormData}
          title="Authenticate!"
          accessibilityLabel="Authenticate"
        />
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      backgroundColor: '#FF3366',
      //paddingTop: 23,
   },
   input: {
      margin: 5,
      height: 36,
      padding: 4,
      fontSize: 18,
      borderWidth: 1,
      borderColor: '#48BBEC',
      borderRadius: 8,
      color: '#48BBEC',
      backgroundColor: '#cc0000'
   },
   pickerHeader: {
      height: 24,
      padding: 4,
      fontSize: 16,
      color: '#48BBEC',
      backgroundColor: '#cc0000',
      alignItems: 'stretch',
   },
   picker: {
      flex: 1,
//      flexDirection: 'column',
//      alignItems: 'center',
//      justifyContent: 'center',
      margin: 0,
      width: 150,
      backgroundColor: '#dd00dd'
   },
   h2: {
      color: 'blue',
      fontWeight: 'bold',
      fontSize: 18,
   },
   error: {
      color: 'red',
      fontWeight: 'bold',
      fontSize: 18,
      margin: 20,
   },
  login: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: 'lightgray',
  }
})

export default connect(mapStateToProps)(GenericForm)
