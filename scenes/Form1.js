import React, { Component } from 'react'
import { View, ScrollView, Text, TextInput, Image, Button, Picker, DatePickerIOS, ActivityIndicator, StyleSheet} from 'react-native'
import { connect } from 'react-redux'

import { FEED_BACK_FORM_ID } from './constants'

import { actionCreators } from './genericFormRedux'

export const types = {
  radio: 'radio',
  textarea: 'textarea',
  dateTime: 'dateTime',
};

const mapStateToProps = (state) => ({
  loading: state.form.loading,
  init: state.form.init,
  formId: state.form.formId,
  formMetadata: state.form.formMetadata,
  formFields: state.form.formFields,
  fieldValues: state.form.fieldValues,
  error: state.form.error,
  errorMessage: state.form.errorMessage,
});


class Form1 extends Component {
  componentDidMount () {
    console.log('Form1.componentDidMount');
  }

  componentWillMount() {
    console.log('Form1.componentWillMount');
    const {dispatch} = this.props
    this.init();
  }

  init = async () => {
    const {dispatch} = this.props;

    // We can await the completion of dispatch, so long as we returned a promise.
    await dispatch(actionCreators.init(FEED_BACK_FORM_ID));
  }

  submitFormData = async () => {
    const {dispatch} = this.props;

    // We can await the completion of dispatch, so long as we returned a promise.
    await dispatch(actionCreators.submitFormData(FEED_BACK_FORM_ID));
  }

  updateFieldValue = async (fieldId, value) => {
    console.log('updateFieldValue->', fieldId, value);
    const {dispatch} = this.props;
    await dispatch(actionCreators.updateFieldValue(fieldId, value));
  }

  render() {
    const {loading, error, errorMessage, init, formMetadata, formFields, fieldValues} = this.props;

    if (loading) {
      return (
        <View style={styles.center}>
          <ActivityIndicator animating={true} />
        </View>
      )
    }

    console.log('Form1.render this.props', this.props);

    return (
     <ScrollView contentContainerStyle={styles.container}>
        <Text style={styles.error} bold>Hi there</Text>
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'column',
      backgroundColor: '#FF3366',
      //paddingTop: 23,
   },
   input: {
      margin: 5,
      height: 36,
      padding: 4,
      fontSize: 18,
      borderWidth: 1,
      borderColor: '#48BBEC',
      borderRadius: 8,
      color: '#48BBEC',
      backgroundColor: '#cc0000'
   },
   pickerHeader: {
      height: 24,
      padding: 4,
      fontSize: 16,
      color: '#48BBEC',
      backgroundColor: '#cc0000',
      alignItems: 'stretch',
   },
   picker: {
      flex: 1,
//      flexDirection: 'column',
//      alignItems: 'center',
//      justifyContent: 'center',
      margin: 0,
      width: 150,
      backgroundColor: '#dd00dd'
   },
   h2: {
      color: 'blue',
      fontWeight: 'bold',
      fontSize: 18,
   },
   error: {
      color: 'red',
      fontWeight: 'bold',
      fontSize: 18,
      margin: 20,
   },
  login: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: 'lightgray',
  }
})

export default connect(mapStateToProps)(Form1);
