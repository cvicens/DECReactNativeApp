import React, { Component } from 'react'
import { View, ScrollView, Text, TextInput, TouchableOpacity, Image, Button, Picker, DatePickerIOS, ActivityIndicator, StyleSheet} from 'react-native'
import { connect } from 'react-redux'

import { FEED_BACK_FORM_ID } from './constants'

import { actionCreators } from './genericFormRedux'

export const types = {
  radio: 'radio',
  radioOld: 'radioOld',
  textarea: 'textarea',
  text: 'text',
  number: 'number',
  dateTime: 'dateTime',
  emailAddress: 'emailAddress'
}

const mapStateToProps = (state) => ({
  loading: state.form.loading,
  init: state.form.init,
  formId: state.form.formId,
  formMetadata: state.form.formMetadata,
  formFields: state.form.formFields,
  fieldValues: state.form.fieldValues,
  error: state.form.error,
  errorMessage: state.form.errorMessage,
})

class PickerButton extends React.Component {
  constructor(props) {
    super(props);

    this._textStyle = { fontSize: props.fontSize };
    this._viewStyle = { flex: props.flex };
  }

  border = (color) =>  { 
    return { borderColor: color, borderWidth: 3 }; 
  }

  renderOld() {
    return (
      <Button onPress={this.props.onPress} title={this.props.title} />
    );
  }

  render() {
    return (
      <TouchableOpacity style={[this._viewStyle]} onPress={this.props.onPress} >
        <View >
          <Text style={this._textStyle}>{this.props.title}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

class PickerWithHeader extends Component {
  _pickerButtonDefaults = {
    fontSize: 15,
    flex: 3
  };

  _styles = StyleSheet.create({
    container: {
      //flex: 1, 
      flexDirection: 'column', 
      alignItems: 'flex-start',
    },
    headerWrapper: { 
      flex: 1,
      //height: 30,
      flexDirection: 'row',
      justifyContent: 'space-around'
    },
    headerLabel: { 
      flex: 3,
      fontSize: 15
    },
    headerValue: { 
      flex: 2,
      fontSize: 15
    },
    pickerWrapper: { 
      height: 200,
      flexDirection: 'row'
    },
    picker: {
      flex: 1, // takes up all the row...
      overflow:'hidden',
      marginTop:-10,
      height: 200
   }
  });

  border = (color) =>  { 
    return { borderColor: color, borderWidth: 3 }; 
  }

  onValueChange = (event) => {};

  constructor(props) {
    super(props);

    this.state = { flipped: false };

    this.onValueChange = this.onValueChange.bind(this);
  }

  handlePress = () => {
    this.setState({flipped: !this.state.flipped});
  };

  render() {
    const {field, selectedValue, onValueChange} = this.props;

    var options = field.fieldOptions.definition.options;
    let items = options.map((option, j) => {
      return (<Picker.Item label={option.label} value={option.label} key={j+'D'}/>);
    });
    //console.log('_id',field._id,'options', options, 'items', items);

    if (!this.state.flipped) {
      return (
            // container
            <View key={field._id+'A'} style={[this._styles.container, this.border('red')]}> 
              
              <View key={field._id+'B'} style={[this._styles.headerWrapper, this.border('blue')]}>
                <PickerButton key={field._id+'B1'} fontSize={this._pickerButtonDefaults.fontSize} flex={this._pickerButtonDefaults.flex} onPress={() => this.setState({flipped: !this.state.flipped})} title={field.name} />

                <Text key={field._id+'B2'} style={this._styles.headerValue}>{selectedValue}</Text>
              </View>
              
            </View>
          );
    }

    return (
      // container
      <View key={field._id+'A'} style={[this._styles.container, this.border('red')]}> 
        
        <View key={field._id+'B'} style={[this._styles.headerWrapper, this.border('blue')]}>
          <PickerButton key={field._id+'B1'} fontSize={this._pickerButtonDefaults.fontSize} flex={this._pickerButtonDefaults.flex} onPress={() => this.setState({flipped: !this.state.flipped})} title={field.name} />

          <Text key={field._id+'B2'} style={this._styles.headerValue}>{selectedValue}</Text>
        </View>
        
        <View key={field._id+'C'} style={[this._styles.pickerWrapper, this.border('green')]}>
          <Picker style={[this._styles.picker]} key={field._id+'E'}
              selectedValue={selectedValue}
              onValueChange={onValueChange}>
            {items}
          </Picker>
        </View>
      </View>
    );
  }
}

class GenericForm extends Component {
  componentDidMount () {
    console.log('GenericForm.componentDidMount');
  }

  componentWillMount() {
    console.log('GenericForm.componentWillMount');
    const {dispatch} = this.props
    this.init();
  }

  init = async () => {
    const {dispatch} = this.props;

    // We can await the completion of dispatch, so long as we returned a promise.
    await dispatch(actionCreators.init(FEED_BACK_FORM_ID));
  }

  submitFormData = async () => {
    const {dispatch, fieldValues} = this.props;

    // We can await the completion of dispatch, so long as we returned a promise.
    await dispatch(actionCreators.submitFormData(FEED_BACK_FORM_ID, fieldValues));
  }

  updateFieldValue = async (fieldId, value) => {
    console.log('updateFieldValue->', fieldId, value);
    const {dispatch} = this.props;
    await dispatch(actionCreators.updateFieldValue(fieldId, value));
  }

  render() {
    const {loading, error, errorMessage, init, formMetadata, formFields, fieldValues} = this.props;

    if (loading) {
      return (
        <View style={styles.center}>
          <ActivityIndicator animating={true} />
        </View>
      )
    }

    //console.log('GenericForm.render this.props', this.props);
    //console.log('GenericForm.render formMetadata', formMetadata);
    //console.log('GenericForm.render formFields', formFields);

    let formComponent = formFields.map((field, i) => {
      switch (field.type) {
        case types.radio: {
          return (
            <PickerWithHeader 
              key={field._id} 
              field={field} 
              selectedValue={fieldValues[field._id]}
              onValueChange={(event) => this.updateFieldValue(field._id, event)}/>
          );
        }
        case types.textarea: {
          return <TextInput key={field._id} style={styles.inputMultiline} autoCapitalize = 'none'
                            multiline = {true}
                            numberOfLines = {4} 
                            placeholder={ field.name } defaultValue={fieldValues[field._id]}
                            onSubmitEditing={(event) => this.updateFieldValue(field._id, event.nativeEvent.text)}
                            onEndEditing={(event) => this.updateFieldValue(field._id, event.nativeEvent.text)}/>
        }
        case types.text: {
          return <TextInput key={field._id} style={styles.input} autoCapitalize = 'none'
                            placeholder={ field.name }
                            defaultValue={fieldValues[field._id]}
                            onSubmitEditing={(event) => this.updateFieldValue(field._id, event.nativeEvent.text)}
                            onEndEditing={(event) => this.updateFieldValue(field._id, event.nativeEvent.text)}/>
        }
        case types.number: {
          return <TextInput key={field._id} style={styles.input} autoCapitalize = 'none' keyboardType='numeric'
                            placeholder={ field.name }
                            defaultValue={fieldValues[field._id]}
                            onSubmitEditing={(event) => this.updateFieldValue(field._id, event.nativeEvent.text)}
                            onEndEditing={(event) => this.updateFieldValue(field._id, event.nativeEvent.text)}/>
        }
        case types.emailAddress: {
          return <TextInput key={field._id} style={styles.input} autoCapitalize = 'none' keyboardType='email-address'
                            placeholder={ field.name }
                            defaultValue={fieldValues[field._id]}
                            onSubmitEditing={(event) => this.updateFieldValue(field._id, event.nativeEvent.text)}
                            onEndEditing={(event) => this.updateFieldValue(field._id, event.nativeEvent.text)}/>
        }
        default: {
          return <Text key={field._id} style={styles.pickerHeader}>Component not available</Text>
        }
      }                            
    }); 
    //console.log('form', formComponent);

    return (
     <ScrollView contentContainerStyle={styles.container}>
        {formComponent}
        <Button style={styles.button}
          disabled={loading}
          onPress={this.submitFormData}
          title="Submit"
          accessibilityLabel="Submit"
        />
      </ScrollView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
      //flex: 1,
      //alignItems: 'center',
      //justifyContent: 'center',
      flexDirection: 'column',
      //backgroundColor: '#FF3366',
      paddingTop: 23,
   },
   input: {
      margin: 5,
      height: 36,
      padding: 4,
      fontSize: 18,
      borderWidth: 1,
      borderColor: '#48BBEC',
      borderRadius: 8,
      //color: '#48BBEC',
      //backgroundColor: '#cc0000'
   },
   inputMultiline: {
      margin: 5,
      height: 36*3,
      padding: 4,
      fontSize: 18,
      borderWidth: 1,
      borderColor: '#48BBEC',
      borderRadius: 8,
   },
   pickerHeader: {
      height: 24,
      padding: 4,
      fontSize: 16,
      color: '#48BBEC',
      backgroundColor: '#cc0000',
      alignItems: 'stretch',
   },
   pickerHeaderValue: {
      height: 24,
      padding: 4,
      fontSize: 16,
      color: '#48BBFF',
      backgroundColor: '#cc0000',
      alignItems: 'flex-end',
   },
   picker: {
      flex: 1,
//      flexDirection: 'column',
//      alignItems: 'center',
//      justifyContent: 'center',
      margin: 0,
      height: 200,
      //width: 150,
      backgroundColor: '#dd00dd'
   },
   h2: {
      color: 'blue',
      fontWeight: 'bold',
      fontSize: 18,
   },
   error: {
      color: 'red',
      fontWeight: 'bold',
      fontSize: 18,
      margin: 20,
   },
  login: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: 'lightgray',
  }
})

export default connect(mapStateToProps)(GenericForm)
