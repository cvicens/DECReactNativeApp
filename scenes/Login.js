import React, { Component } from 'react'
import { View, ScrollView, Text, TextInput, Image, Button, ActivityIndicator, StyleSheet} from 'react-native'
import { connect } from 'react-redux'


import { actionCreators } from './loginRedux'

import { LOGIN_IMAGE } from './constants'

const mapStateToProps = (state) => ({
  loading: state.login.loading,
  error: state.login.error,
  errorMessage: state.login.errorMessage,
  username: state.login.username,
  password: state.login.password,
  init: state.login.init,
  token: state.login.token,
  authenticated: state.login.authenticated,
  userId: state.login.userId
})

class WithLabel extends React.Component {
  render() {
    return (
      <View style={styles.labelContainer}>
        <View style={styles.label}>
          <Text>{this.props.label}</Text>
        </View>
        {this.props.children}
      </View>
    );
  }
}

class Login extends Component {
  componentDidMount () {
    console.log('Login.componentDidMount');
  }

  componentWillMount() {
    console.log('Login.componentWillMount');
    const {dispatch} = this.props
    this.init();
  }

  init = async () => {
    const {dispatch} = this.props;

    // We can await the completion of dispatch, so long as we returned a promise.
    await dispatch(actionCreators.clear());
  }

  onAuthenticate = async () => {
    const {dispatch, username, password} = this.props;

    // We can await the completion of dispatch, so long as we returned a promise.
    await dispatch(actionCreators.auth('wfm', username, password));

    const {navigator, error} = this.props;

    if (!error) {
      //navigator.push({id: 'form1'});
      navigator.push({id: 'generic'});
    }
  }

  updateUsername = async (username) => {
    const {dispatch} = this.props;
    await dispatch(actionCreators.updateUsername(username));
  }

  updatePassword = async (password) => {
    const {dispatch} = this.props;
    await dispatch(actionCreators.updatePassword(password));
  }

  render() {
    const {loading, error, errorMessage, init, authenticated, userId, username, password} = this.props

    console.log('Login.render this.props', this.props);

    return (

     <Image key='1'
        source={LOGIN_IMAGE}
        style={styles.container}>
        <TextInput key='2' style={styles.input} autoCapitalize = 'none'
            onSubmitEditing={(event) => this.updateUsername(event.nativeEvent.text)}
            onEndEditing={(event) => this.updateUsername(event.nativeEvent.text)}
            placeholder='User Name'
            placeholderTextColor='white'
          />
        <TextInput key='3' style={styles.input} secureTextEntry={true} autoCapitalize = 'none'
          onSubmitEditing={(event) => this.updatePassword(event.nativeEvent.text)}
          onEndEditing={(event) => this.updatePassword(event.nativeEvent.text)}
          placeholder='Password'
          placeholderTextColor='white'
        />
        <Text key='4' style={styles.error} bold>{errorMessage}</Text>
        <Button key='5' style={styles.button}
          disabled={loading}
          onPress={this.onAuthenticate}
          title="Authenticate!"
          accessibilityLabel="Authenticate"
          color='white'
        />
      </Image>

    )
  }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      alignItems: 'center',
      justifyContent:'center',
      //paddingTop: 23,
      width: undefined,
      height: undefined,
      backgroundColor:'transparent',
   },
   input: {
      margin: 30,
      height: 36,
      padding: 4,
      fontSize: 18,
      borderWidth: 1,
      borderColor: '#ffffff',
      borderRadius: 8,
      color: '#ffffff'
   },
   h2: {
      color: 'blue',
      fontWeight: 'bold',
      fontSize: 18,
   },
   error: {
      color: '#fd3e6a',
      fontWeight: 'bold',
      fontSize: 18,
      margin: 20,
   },
  login: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: 'lightgray',
  },
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover', // or 'stretch'
  },
})

export default connect(mapStateToProps)(Login)
