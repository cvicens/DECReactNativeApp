///var RCTFH = require('rct-fh');
//import { NativeModules } from 'react-native';
//var RCTFH = NativeModules.FH;
//console.log('NativeModules', NativeModules);

export const types = {
  GET_FORM_DETAIL_REQUEST: 'GET_FORM_DETAIL_REQUEST',
  GET_FORM_DETAIL_RESPONSE: 'GET_FORM_DETAIL_RESPONSE',
  INIT: 'INIT',
  CLEAR: 'CLEAR',
}

export const actionCreators = {
  init: (formId) => async (dispatch, getState) => {
    console.log('init Form Metadata', dispatch, getState);
    dispatch({type: types.GET_FORM_DETAIL_REQUEST});

    try {
      const payload = {
        formDetail: {
          pages: [
            {
              fields: [
                  {
                  name: 'Valoración',
                  type: 'radio',
                  fieldOptions: {
                    definition: {
                      options: [
                        {
                          label: 'Excelente'
                        },
                        {
                          label: 'Buena'
                        }
                      ]
                    }
                  }
                },
                {
                  type: 'textarea',
                  name: 'XYZ'
                },
                {
                  name: 'Repetiría?',
                  type: 'radio',
                  fieldOptions: {
                    definition: {
                      options: [
                        {
                          label: 'Sí'
                        },
                        {
                          label: 'No'
                        }
                      ]
                    }
                  }
                },
                {
                  type: 'textarea',
                  name: 'First Name'
                },
                {
                  type: 'textarea',
                  name: 'Last Name'
                }
              ]
            }
          ]
        }
      };

      dispatch({type: types.GET_FORM_DETAIL_RESPONSE, payload: payload})
    } catch (e) {
      dispatch({type: types.GET_FORM_DETAIL_RESPONSE, payload: e, error: true})
    }
  },

  clear: () => async (dispatch, getState) => {
    dispatch({type: types.CLEAR});
  }
}

const initialState = {
  loading: false,
  init: false,
  formId: '',
  formMetadata: {},
  formFields: [],
  error: false,
  errorMessage: '',
}

export const components = (state = initialState, action) => {
  const {formMetadata} = state
  const {type, payload, error} = action

  switch (type) {
    case types.GET_FORM_DETAIL_REQUEST: {
      return {...state, loading: true}
    }
    case types.GET_FORM_DETAIL_RESPONSE: {
      if (error) {
        return {...state, loading: false, error: true, init: false, errorMessage: payload.message}
      }
      return {...state, loading: false, error: false, init: true, formMetadata: payload.formDetail, formFields: payload.formDetail.pages[0].fields}
    }
  }

  return state
}
