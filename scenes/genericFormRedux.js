var RCTFH = require('rct-fh');
//import { NativeModules } from 'react-native';
//var RCTFH = NativeModules.FH;
//console.log('NativeModules', NativeModules);

export const types = {
  GET_FORM_DETAIL_REQUEST: 'GET_FORM_DETAIL_REQUEST',
  GET_FORM_DETAIL_RESPONSE: 'GET_FORM_DETAIL_RESPONSE',
  POST_FORM_DATA_REQUEST: 'POST_FORM_DATA_REQUEST',
  POST_FORM_DATA_RESPONSE: 'POST_FORM_DATA_RESPONSE',
  UPDATE_FIELD_VALUE: 'UPDATE_FIELD_VALUE',
  INIT: 'INIT',
  CLEAR: 'CLEAR',
}

export const actionCreators = {
  init: (formId) => async (dispatch, getState) => {
    //console.log('init Form Metadata', dispatch, getState);
    dispatch({type: types.GET_FORM_DETAIL_REQUEST});

    try {
      const payload = await RCTFH.cloud({
        "path": "/forms/" + formId, //only the path part of the url, the host will be added automatically
        "method": "GET", //all other HTTP methods are supported as well. For example, HEAD, DELETE, OPTIONS
        "contentType": "application/json",
        "data": { "hello": "Carlos"}, //data to send to the server
        "timeout": 25000 // timeout value specified in milliseconds. Default: 60000 (60s)
      });

      dispatch({type: types.GET_FORM_DETAIL_RESPONSE, payload: payload})
    } catch (e) {
      dispatch({type: types.GET_FORM_DETAIL_RESPONSE, payload: e, error: true})
    }
  },

submitFormData: (formId, fieldValues) => async (dispatch, getState) => {
    //console.log('submit Form Data', dispatch, getState);
    dispatch({type: types.POST_FORM_DATA_REQUEST});

    var data = {formId: formId, fields: []};
    for (var key in fieldValues) {
      if (fieldValues.hasOwnProperty(key)) {
        var aux = {fieldId: key, fieldValues: [fieldValues[key]]};
        data.fields.push(aux);
      }
    }

    try {
      const payload = await RCTFH.cloud({
        "path": "/submissions/model", //only the path part of the url, the host will be added automatically
        "method": "POST", //all other HTTP methods are supported as well. For example, HEAD, DELETE, OPTIONS
        "contentType": "application/json",
        "data": data, //data to send to the server
        "timeout": 25000 // timeout value specified in milliseconds. Default: 60000 (60s)
      });

      dispatch({type: types.POST_FORM_DATA_RESPONSE, payload: payload})
      alert('Form submitted correctly with id: ' + payload);
    } catch (e) {
      dispatch({type: types.POST_FORM_DATA_RESPONSE, payload: e, error: true})
      alert(e);
    }
  },

  updateFieldValue: (fieldId, value) => async (dispatch, getState) => {
    //console.log('updateFieldValue', fieldId, 'v:', value);
    dispatch({type: types.UPDATE_FIELD_VALUE, payload: {fieldId: fieldId, value: value}});
  },

  clear: () => async (dispatch, getState) => {
    dispatch({type: types.CLEAR});
  }
}

const initialState = {
  loading: false,
  init: false,
  formId: '',
  formMetadata: {},
  formFields: [],
  fieldValues: {},
  error: false,
  errorMessage: '',
}

export const form = (state = initialState, action) => {
  const {formMetadata, fieldValues} = state
  const {type, payload, error} = action

  switch (type) {
    case types.GET_FORM_DETAIL_REQUEST: {
      return {...state, loading: true}
    }
    case types.GET_FORM_DETAIL_RESPONSE: {
      if (error) {
        return {...state, loading: false, error: true, init: false, errorMessage: payload.message}
      }
      var defaultFieldValues = {};
      var auxFields = payload.formDetail.pages[0].fields;
      for (var i = 0; i < auxFields.length; i++) {
        var field = auxFields[i];
        if (field.type == 'radio' && field.fieldOptions && field.fieldOptions.definition) {
          for (var j = 0; j < field.fieldOptions.definition.options.length; j++) {
            if (field.fieldOptions.definition.options[j].checked) {
              defaultFieldValues[field._id] = field.fieldOptions.definition.options[j].label;
            }
          }
        }
        if (field.type == 'text' || field.type == 'textarea') {
          if (field.fieldOptions && field.fieldOptions.definition && field.fieldOptions.definition.defaultValue) {
            defaultFieldValues[field._id] = field.fieldOptions.definition.defaultValue;
          }
        }
      }
      return {...state, loading: false, error: false, init: true, formMetadata: payload.formDetail, formFields: payload.formDetail.pages[0].fields, fieldValues: defaultFieldValues}
    }
    case types.UPDATE_FIELD_VALUE: {
      //fieldValues[payload.fieldId] = payload.value;
      var newFieldValues = {...fieldValues};
      newFieldValues[payload.fieldId] = payload.value;
      return {...state, fieldValues: newFieldValues}
    }
  }

  return state
}
