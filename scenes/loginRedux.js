var RCTFH = require('rct-fh');
//import { NativeModules } from 'react-native';
//var RCTFH = NativeModules.FH;
//console.log('NativeModules', NativeModules);

export const types = {
  UPDATE_USERNAME: 'UPDATE_USERNAME',
  UPDATE_PASSWORD: 'UPDATE_PASSWORD',
  FH_SDK_AUTH_REQUEST: 'FH_SDK_AUTH_REQUEST',
  FH_SDK_AUTH_RESPONSE: 'FH_SDK_AUTH_RESPONSE',
  CLEAR: 'CLEAR',
}

export const actionCreators = {
  auth: (authPolicy, username, password) => async (dispatch, getState) => {
    console.log('auth FH SDK', dispatch, getState);
    dispatch({type: types.FH_SDK_AUTH_REQUEST});

    try {
        const result = await RCTFH.auth(authPolicy, username, password);
        console.log('auth result', result);
        if (typeof result.sessionToken !== 'undefined') {
          dispatch({type: types.FH_SDK_AUTH_RESPONSE, payload: result});
        } else {
          dispatch({type: types.FH_SDK_AUTH_RESPONSE, payload: result, error: true});
        }
      } catch (e) {
        dispatch({type: types.FH_SDK_AUTH_RESPONSE, payload: e, error: true});
      }
  },

  updateUsername: (username) => async (dispatch, getState) => {
    console.log('updateUsername', username);
    dispatch({type: types.UPDATE_USERNAME, payload: username});
  },

  updatePassword: (password) => async (dispatch, getState) => {
    console.log('updatePassword', password);
    dispatch({type: types.UPDATE_PASSWORD, payload: password});
  },

  clear: () => async (dispatch, getState) => {
    dispatch({type: types.CLEAR})
  }
}

const initialState = {
  loading: false,
  authenticated: false,
  init: false,
  username: '',
  password: '',
  userId: '',
  token: {},
  error: false,
  errorMessage: '',
}

export const login = (state = initialState, action) => {
  const {todos} = state
  const {type, payload, error} = action

  switch (type) {
    case types.UPDATE_USERNAME: {
      return {...state, username: payload}
    }
    case types.UPDATE_PASSWORD: {
      return {...state, password: payload}
    }
    case types.FH_SDK_AUTH_REQUEST: {
      return {...state, loading: true, authenticated: false}
    }
    case types.FH_SDK_AUTH_RESPONSE: {
      if (error) {
        return {...state, loading: false, authenticated: false, error: true, errorMessage: payload.message}
      }
      return {...state, loading: false, authenticated: true, error: false, userId: payload.userId}
    }
  }

  return state
}
